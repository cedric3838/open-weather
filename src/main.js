import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import moment from 'moment';
import 'moment/locale/fr'
moment.locale('fr')

Vue.prototype.moment = moment;

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')